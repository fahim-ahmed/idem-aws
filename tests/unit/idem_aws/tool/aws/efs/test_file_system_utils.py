def test_file_system_utils(hub):
    # When current state is empty, is throughput mode update should be true
    current_state = {}
    desired_state = {
        "file_system_name": "idem-test-fs",
        "performance_mode": "generalPurpose",
        "throughput_mode": "bursting",
        "provisioned_throughput_in_mibps": 500.0,
    }

    result = hub.tool.aws.efs.file_system_utils.is_throughput_mode_updated(
        current_state, desired_state
    )
    assert result

    # When desired state throughput mode is same as current state, is throughput mode updated should be false
    current_state = {
        "file_system_name": "idem-test-fs",
        "performance_mode": "generalPurpose",
        "throughput_mode": "bursting",
        "provisioned_throughput_in_mibps": 500.0,
    }
    desired_state = {
        "file_system_name": "idem-test-fs",
        "performance_mode": "generalPurpose",
        "throughput_mode": "bursting",
        "provisioned_throughput_in_mibps": 500.0,
    }

    result = hub.tool.aws.efs.file_system_utils.is_throughput_mode_updated(
        current_state, desired_state
    )
    assert not result

    # When desired state throughput mode is different from current state, is throughput mode updated should be true
    current_state = {
        "file_system_name": "idem-test-fs",
        "performance_mode": "generalPurpose",
        "throughput_mode": "bursting",
    }
    desired_state = {
        "file_system_name": "idem-test-fs-",
        "performance_mode": "generalPurpose",
        "throughput_mode": "provisioned",
        "provisioned_throughput_in_mibps": 500.0,
    }

    result = hub.tool.aws.efs.file_system_utils.is_throughput_mode_updated(
        current_state, desired_state
    )
    assert result

    # backup result is false, return false
    backup_result = {"result": False}
    result = hub.tool.aws.efs.file_system_utils.is_backup_enabled(backup_result)
    assert not result

    # backup result is true and status is Enabling, return false
    backup_result = {"result": True, "ret": {"BackupPolicy": {"Status": "ENABLING"}}}
    result = hub.tool.aws.efs.file_system_utils.is_backup_enabled(backup_result)
    assert result

    # backup result is true and status is Enabled, return false
    backup_result = {"result": True, "ret": {"BackupPolicy": {"Status": "ENABLED"}}}
    result = hub.tool.aws.efs.file_system_utils.is_backup_enabled(backup_result)
    assert result
