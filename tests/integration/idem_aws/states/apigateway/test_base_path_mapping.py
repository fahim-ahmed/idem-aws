import asyncio
import copy
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="present")
async def test_present(
    hub,
    ctx,
    __test,
    aws_apigateway_domain_name,
    aws_api_gateway,
    cleanup,
):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["domain_name"] = aws_apigateway_domain_name["name"]
    PARAMETER["name"] = aws_apigateway_domain_name["name"]
    PARAMETER["stage"] = aws_api_gateway["stage"]["stageName"]
    PARAMETER["rest_api_id"] = aws_api_gateway["ret"]["id"]
    PARAMETER["base_path"] = "vmware"
    would_create_message = hub.tool.aws.comment_utils.would_create_comment(
        resource_type="aws.apigateway.base_path_mapping",
        name=PARAMETER["domain_name"],
    )
    created_message = hub.tool.aws.comment_utils.create_comment(
        resource_type="aws.apigateway.base_path_mapping",
        name=PARAMETER["domain_name"],
    )
    present_ret = await hub.states.aws.apigateway.base_path_mapping.present(
        ctx,
        **PARAMETER,
    )
    created_resource = present_ret["new_state"]
    if __test:
        assert would_create_message == present_ret["comment"]
    else:
        assert created_message == present_ret["comment"]
    assert not present_ret["old_state"] and present_ret["new_state"]
    assert PARAMETER["name"] == present_ret["name"]
    assert PARAMETER["stage"] == created_resource["stage"]
    assert PARAMETER["base_path"] == created_resource["base_path"]
    assert PARAMETER["rest_api_id"] == created_resource["rest_api_id"]
    if not __test:
        PARAMETER["resource_id"] = created_resource["resource_id"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.apigateway.base_path_mapping.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    assert "aws.apigateway.base_path_mapping.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id][
        "aws.apigateway.base_path_mapping.present"
    ]
    described_resource_map = dict(ChainMap(*described_resource))
    assert described_resource_map["stage"] == PARAMETER["stage"]
    assert described_resource_map["rest_api_id"] == PARAMETER["rest_api_id"]
    assert described_resource_map["base_path"] == PARAMETER["base_path"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="exec-get", depends=["present"])
# This test is here to avoid the need to create a base path mapping fixture for testing exec.get()
async def test_get(hub, ctx):
    ret = await hub.exec.aws.apigateway.base_path_mapping.get(
        ctx=ctx, domain_name=PARAMETER["domain_name"], base_path=PARAMETER["base_path"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["stage"] == resource.get("stage")
    assert PARAMETER["base_path"] == resource.get("base_path")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="update_base_path", depends=["describe"])
async def test_update_base_path(hub, ctx, __test, aws_api_gateway):
    # this is to avoid a "TooManyRequestsException" thrown by the API Gateway servers
    # (request is being made too frequently and is more than what the server can handle)
    if not hub.tool.utils.is_running_localstack(ctx):
        await asyncio.sleep(30)
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter["stage"] = aws_api_gateway["stage2"]["stageName"]
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.base_path_mapping.present(
        ctx, **new_parameter
    )
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.apigateway.base_path_mapping",
                name=new_parameter["domain_name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert ret["result"], ret["comment"]
        assert ret["old_state"] and ret["new_state"]
        old_resource = ret["old_state"]
        assert PARAMETER["base_path"] == old_resource.get("base_path")
        assert PARAMETER["rest_api_id"] == old_resource.get("rest_api_id")
        new_resource = ret["new_state"]
        assert new_parameter["base_path"] == new_resource.get("base_path")
        assert new_parameter["stage"] == new_resource.get("stage")
        assert new_parameter["rest_api_id"] == new_resource.get("rest_api_id")
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="absent", depends=["update_base_path"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.base_path_mapping.absent(
        ctx,
        name=PARAMETER["name"],
        domain_name=PARAMETER["domain_name"],
        base_path=PARAMETER["base_path"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert (ret["old_state"]) and (not ret["new_state"])
    if __test:
        if PARAMETER["base_path"]:
            assert (
                hub.tool.aws.comment_utils.would_delete_comment(
                    resource_type="aws.apigateway.base_path_mapping",
                    name=PARAMETER["domain_name"],
                )[0]
                == ret["comment"][0]
            )
        else:
            assert (
                hub.tool.aws.comment_utils.already_absent_comment(
                    resource_type="aws.apigateway.base_path_mapping",
                    name=PARAMETER["domain_name"],
                )[0]
                == ret["comment"][0]
            )
    else:
        if PARAMETER["base_path"]:
            assert (
                hub.tool.aws.comment_utils.delete_comment(
                    resource_type="aws.apigateway.base_path_mapping",
                    name=PARAMETER["domain_name"],
                )[0]
                == ret["comment"][0]
            )
        else:
            assert (
                hub.tool.aws.comment_utils.already_absent_comment(
                    resource_type="aws.apigateway.base_path_mapping",
                    name=PARAMETER["domain_name"],
                )[0]
                == ret["comment"][0]
            )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.base_path_mapping.absent(
        ctx,
        name=PARAMETER["name"],
        domain_name=PARAMETER["domain_name"],
        base_path=PARAMETER["base_path"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigateway.base_path_mapping",
            name=PARAMETER["domain_name"],
        )
        == ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.apigateway.base_path_mapping.absent(
            ctx,
            name=PARAMETER["name"],
            domain_name=PARAMETER["domain_name"],
            base_path=PARAMETER["base_path"],
            resource_id=PARAMETER["resource_id"],
        )
