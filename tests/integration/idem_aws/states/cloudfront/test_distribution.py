import copy
import time
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-distribution-" + str(int(time.time())),
    "caller_reference": "distribution-" + str(int(time.time())),
    "comment": "creating cloudfront distribution for idem test",
    "price_class": "PriceClass_All",
}
STATE_NAME = "aws.cloudfront.distribution"


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, aws_s3_bucket, __test, cleanup):
    # skipping test in localstack as cloudfront distribution creation is not supported in localstack.
    if hub.tool.utils.is_running_localstack(ctx):
        return
    global PARAMETER, STATE_NAME
    ctx["test"] = __test
    bucket = aws_s3_bucket.get("name")

    PARAMETER["origins"] = {
        "Quantity": 1,
        "Items": [
            {
                "Id": bucket + ".s3.us-east-1.amazonaws.com",
                "DomainName": bucket + ".s3.us-east-1.amazonaws.com",
                "OriginPath": "",
                "CustomHeaders": {
                    "Quantity": 0,
                },
                "S3OriginConfig": {"OriginAccessIdentity": ""},
                "ConnectionAttempts": 3,
                "ConnectionTimeout": 10,
                "OriginShield": {
                    "Enabled": False,
                },
                "OriginAccessControlId": "",
            },
        ],
    }
    PARAMETER["default_cache_behaviour"] = {
        "TargetOriginId": bucket + ".s3.us-east-1.amazonaws.com",
        "TrustedSigners": {
            "Enabled": False,
            "Quantity": 0,
        },
        "TrustedKeyGroups": {
            "Enabled": False,
            "Quantity": 0,
        },
        "ViewerProtocolPolicy": "redirect-to-https",
        "AllowedMethods": {
            "Quantity": 2,
            "Items": [
                "HEAD",
                "GET",
            ],
            "CachedMethods": {
                "Quantity": 2,
                "Items": [
                    "HEAD",
                    "GET",
                ],
            },
        },
        "SmoothStreaming": False,
        "Compress": True,
        "LambdaFunctionAssociations": {
            "Quantity": 0,
        },
        "FunctionAssociations": {
            "Quantity": 0,
        },
        "FieldLevelEncryptionId": "",
        "CachePolicyId": "658327ea-f89d-4fab-a63d-7e88639e58f6",
    }
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}
    PARAMETER["enabled"] = True

    ret = await hub.states.aws.cloudfront.distribution.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )

    assert not ret["old_state"] and ret["new_state"]
    assert_distribution(resource, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    # skipping test in localstack as cloudfront distribution creation is not supported in localstack.
    if hub.tool.utils.is_running_localstack(ctx):
        return
    global PARAMETER, STATE_NAME
    describe_ret = await hub.states.aws.cloudfront.distribution.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    # Verify that describe output format is correct
    assert STATE_NAME + ".present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(STATE_NAME + ".present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert_distribution(described_resource_map, PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="disable_distribution", depends=["describe"])
async def test_disable_distribution(hub, ctx, __test):
    # skipping test in localstack as cloudfront distribution creation is not supported in localstack.
    if hub.tool.utils.is_running_localstack(ctx):
        return
    global PARAMETER, STATE_NAME
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test

    new_parameter["price_class"] = "PriceClass_100"
    new_parameter[
        "comment"
    ] = "Modified price_class and disabled cloudfront distribution"
    new_parameter["tags"].update(
        {
            f"idem-test-key-{str(int(time.time()))}": f"idem-test-value-{str(int(time.time()))}",
        }
    )
    new_parameter["enabled"] = False
    ret = await hub.states.aws.cloudfront.distribution.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]

    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )

    old_resource = ret["old_state"]
    assert_distribution(old_resource, PARAMETER)

    resource = ret["new_state"]
    assert_distribution(resource, new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="enable_distribution", depends=["disable_distribution"])
async def test_enable_distribution(hub, ctx, __test):
    # skipping test in localstack as cloudfront distribution creation is not supported in localstack.
    if hub.tool.utils.is_running_localstack(ctx):
        return
    global PARAMETER, STATE_NAME
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test

    new_parameter["tags"] = {"Name": new_parameter["tags"]["Name"]}
    new_parameter["tags"] = {"Name": PARAMETER["name"]}
    new_parameter["enabled"] = True
    ret = await hub.states.aws.cloudfront.distribution.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]

    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )

    old_resource = ret["old_state"]
    assert_distribution(old_resource, PARAMETER)

    resource = ret["new_state"]
    assert_distribution(resource, new_parameter)
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent", depends=["enable_distribution"])
async def test_absent(hub, ctx, __test):
    # skipping test in localstack as cloudfront distribution creation is not supported in localstack.
    if hub.tool.utils.is_running_localstack(ctx):
        return
    ctx["test"] = __test
    ret = await hub.states.aws.cloudfront.distribution.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    assert_distribution(old_resource, PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type=STATE_NAME, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    # skipping test in localstack as cloudfront distribution creation is not supported in localstack.
    if hub.tool.utils.is_running_localstack(ctx):
        return
    ctx["test"] = __test
    ret = await hub.states.aws.cloudfront.distribution.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=STATE_NAME, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
async def test_distribution_absent_with_none_resource_id(hub, ctx):
    distribution_temp_name = "idem-test-distribution-" + str(int(time.time()))
    # Delete distribution with resource_id as None. Result in no-op.
    ret = await hub.states.aws.cloudfront.distribution.absent(
        ctx, name=distribution_temp_name, resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=STATE_NAME, name=distribution_temp_name
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.cloudfront.distribution.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]


def assert_distribution(resource, parameters):
    assert parameters.get("caller_reference") == resource.get("caller_reference")
    assert parameters.get("origins") == resource.get("origins")
    assert parameters.get("default_cache_behaviour") == resource.get(
        "default_cache_behaviour"
    )
    assert parameters.get("comment") == resource.get("comment")
    assert parameters.get("enabled") == resource.get("enabled")
    assert parameters.get("price_class") == resource.get("price_class")
    assert parameters.get("tags") == resource.get("tags")
