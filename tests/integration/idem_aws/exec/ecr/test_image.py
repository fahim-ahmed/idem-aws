import time
from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR images")
@pytest.mark.dependency(name="list-empty-repository")
async def test_list_empty_repository(hub, ctx, aws_ecr_repository):
    registry_id = aws_ecr_repository.get("registry_id")
    repository_name = aws_ecr_repository.get("repository_name")
    ret = await hub.exec.aws.ecr.image.list(
        ctx, repository_name=repository_name, registry_id=registry_id
    )
    assert ret["result"], ret["comment"]
    assert not ret["ret"]
    assert hub.tool.aws.comment_utils.list_empty_comment(
        resource_type="aws.ecr.image",
        name=repository_name,
    ) in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR images")
@pytest.mark.dependency(name="get-image-by-tag", depends=["list-empty-repository"])
async def test_get_image_by_tag(hub, ctx, aws_ecr_image):
    name = "idem-test-ecr-image-" + str(int(time.time()))
    registry_id = aws_ecr_image.get("registry_id")
    repository_name = aws_ecr_image.get("repository_name")
    image_digest = aws_ecr_image.get("image_digest")
    image_tags = aws_ecr_image.get("image_tags")
    image_size_in_bytes = aws_ecr_image.get("image_size_in_bytes")
    image_manifest_media_type = aws_ecr_image.get("image_manifest_media_type")
    artifact_media_type = aws_ecr_image.get("artifact_media_type")
    ret = await hub.exec.aws.ecr.image.get(
        ctx,
        name=name,
        repository_name=repository_name,
        registry_id=registry_id,
        image_tag=image_tags[0],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert name == resource.get("name")
    assert registry_id == resource.get("registry_id")
    assert repository_name == resource.get("repository_name")
    assert image_digest == resource.get("image_digest")
    assert image_tags == resource.get("image_tags")
    assert image_size_in_bytes == resource.get("image_size_in_bytes")
    assert image_manifest_media_type == resource.get("image_manifest_media_type")
    assert artifact_media_type == resource.get("artifact_media_type")


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR images")
@pytest.mark.dependency(name="get-image-by-digest", depends=["get-image-by-tag"])
async def test_get_image_by_digest(hub, ctx, aws_ecr_image):
    name = "idem-test-ecr-image-" + str(int(time.time()))
    registry_id = aws_ecr_image.get("registry_id")
    repository_name = aws_ecr_image.get("repository_name")
    image_digest = aws_ecr_image.get("image_digest")
    image_tags = aws_ecr_image.get("image_tags")
    image_size_in_bytes = aws_ecr_image.get("image_size_in_bytes")
    image_manifest_media_type = aws_ecr_image.get("image_manifest_media_type")
    artifact_media_type = aws_ecr_image.get("artifact_media_type")
    ret = await hub.exec.aws.ecr.image.get(
        ctx,
        name=name,
        repository_name=repository_name,
        registry_id=registry_id,
        image_digest=image_digest,
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert name == resource.get("name")
    assert registry_id == resource.get("registry_id")
    assert repository_name == resource.get("repository_name")
    assert image_digest == resource.get("image_digest")
    assert image_tags == resource.get("image_tags")
    assert image_size_in_bytes == resource.get("image_size_in_bytes")
    assert image_manifest_media_type == resource.get("image_manifest_media_type")
    assert artifact_media_type == resource.get("artifact_media_type")


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR images")
@pytest.mark.dependency(
    name="get-missing-image-tag-digest", depends=["get-image-by-digest"]
)
async def test_get_missing_image_tag_digest(hub, ctx, aws_ecr_image):
    name = "idem-test-ecr-image-" + str(int(time.time()))
    registry_id = aws_ecr_image.get("registry_id")
    repository_name = aws_ecr_image.get("repository_name")
    ret = await hub.exec.aws.ecr.image.get(
        ctx, name=name, repository_name=repository_name, registry_id=registry_id
    )
    assert not ret["result"], ret["comment"]
    assert not ret["ret"]
    assert "Either `image_digest` or `image_tag` has to be provided" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR images")
@pytest.mark.dependency(
    name="get-image-not-found", depends=["get-missing-image-tag-digest"]
)
async def test_get_image_not_found(hub, ctx, aws_ecr_image):
    name = "idem-test-ecr-image-" + str(int(time.time()))
    registry_id = aws_ecr_image.get("registry_id")
    repository_name = aws_ecr_image.get("repository_name")
    image_tag = "fake-image-tag-" + str(int(time.time()))
    ret = await hub.exec.aws.ecr.image.get(
        ctx,
        name=name,
        repository_name=repository_name,
        registry_id=registry_id,
        image_tag=image_tag,
    )
    assert ret["result"], ret["comment"]
    assert not ret["ret"]
    assert hub.tool.aws.comment_utils.get_empty_comment(
        resource_type="aws.ecr.image",
        name=name,
    ) in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR images")
@pytest.mark.dependency(
    name="get-repository-not-found", depends=["get-image-not-found"]
)
async def test_get_repository_not_found(hub, ctx, aws_ecr_image):
    name = "idem-test-ecr-image-" + str(int(time.time()))
    registry_id = aws_ecr_image.get("registry_id")
    repository_name = "fake-repository-name-" + str(int(time.time()))
    image_digest = aws_ecr_image.get("image_digest")
    ret = await hub.exec.aws.ecr.image.get(
        ctx,
        name=name,
        repository_name=repository_name,
        registry_id=registry_id,
        image_digest=image_digest,
    )
    assert ret["result"], ret["comment"]
    assert not ret["ret"]
    assert hub.tool.aws.comment_utils.get_empty_comment(
        resource_type="aws.ecr.image",
        name=name,
    ) in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR images")
@pytest.mark.dependency(
    name="get-invalid-registry", depends=["get-repository-not-found"]
)
async def test_get_invalid_registry(hub, ctx, aws_ecr_image):
    name = "idem-test-ecr-image-" + str(int(time.time()))
    registry_id = "123456789012"
    repository_name = aws_ecr_image.get("repository_name")
    image_digest = aws_ecr_image.get("image_digest")
    ret = await hub.exec.aws.ecr.image.get(
        ctx,
        name=name,
        repository_name=repository_name,
        registry_id=registry_id,
        image_digest=image_digest,
    )
    assert not ret["result"], ret["comment"]
    assert not ret["ret"]


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR images")
@pytest.mark.dependency(name="list", depends=["get-invalid-registry"])
async def test_list(hub, ctx, aws_ecr_image):
    registry_id = aws_ecr_image.get("registry_id")
    repository_name = aws_ecr_image.get("repository_name")
    image_digest = aws_ecr_image.get("image_digest")
    image_tags = aws_ecr_image.get("image_tags")
    image_size_in_bytes = aws_ecr_image.get("image_size_in_bytes")
    image_manifest_media_type = aws_ecr_image.get("image_manifest_media_type")
    artifact_media_type = aws_ecr_image.get("artifact_media_type")
    ret = await hub.exec.aws.ecr.image.list(
        ctx, repository_name=repository_name, registry_id=registry_id
    )
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    resource = list(
        filter(lambda image: image["image_digest"] == image_digest, ret["ret"])
    )
    assert resource
    assert registry_id == resource[0].get("registry_id")
    assert repository_name == resource[0].get("repository_name")
    assert image_digest == resource[0].get("image_digest")
    assert image_tags == resource[0].get("image_tags")
    assert image_size_in_bytes == resource[0].get("image_size_in_bytes")
    assert image_manifest_media_type == resource[0].get("image_manifest_media_type")
    assert artifact_media_type == resource[0].get("artifact_media_type")


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR images")
@pytest.mark.dependency(name="list-repository-not-found", depends=["list"])
async def test_list_repository_not_found(hub, ctx, aws_ecr_image):
    registry_id = aws_ecr_image.get("registry_id")
    repository_name = "fake-repository-name-" + str(int(time.time()))
    ret = await hub.exec.aws.ecr.image.list(
        ctx, repository_name=repository_name, registry_id=registry_id
    )
    assert not ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    assert len(ret["ret"]) == 0


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR images")
@pytest.mark.dependency(
    name="list-invalid-registry", depends=["list-repository-not-found"]
)
async def test_list_invalid_registry(hub, ctx, aws_ecr_image):
    registry_id = "123456789012"
    repository_name = aws_ecr_image.get("repository_name")
    ret = await hub.exec.aws.ecr.image.list(
        ctx, repository_name=repository_name, registry_id=registry_id
    )
    assert not ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    assert len(ret["ret"]) == 0
