import uuid
from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR repositories")
async def test_list_registry_not_found(hub, ctx):
    registry_id = "123456789012"
    ret = await hub.exec.aws.ecr.repository.list(ctx, registry_id=registry_id)
    assert not ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    assert len(ret["ret"]) == 0


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR repositories")
async def test_list(hub, ctx, aws_test_ecr_repository):
    ret = await hub.exec.aws.ecr.repository.list(
        ctx, repository_names=None, registry_id=None
    )
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    resources = list(
        filter(
            lambda repository: repository["name"] == aws_test_ecr_repository, ret["ret"]
        )
    )
    assert resources
    assert resources[0].get("registry_id")
    assert aws_test_ecr_repository == resources[0].get("repository_name")
    assert resources[0].get("image_tag_mutability")
    assert resources[0].get("image_scanning_configuration")
    assert resources[0].get("tags")


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR repositories")
async def test_list_with_repository_filter(hub, ctx, aws_test_ecr_repository):
    ret = await hub.exec.aws.ecr.repository.list(
        ctx, repository_names=[aws_test_ecr_repository], registry_id=None
    )
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    resources = ret["ret"]
    assert resources
    assert len(resources) == 1
    assert resources[0].get("registry_id")
    assert aws_test_ecr_repository == resources[0].get("repository_name")
    assert resources[0].get("image_tag_mutability")
    assert resources[0].get("image_scanning_configuration")
    assert resources[0].get("tags")


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR repositories")
async def test_get_invalid_repository(hub, ctx):
    fake_repository_name = str(uuid.uuid4())
    ret = await hub.exec.aws.ecr.repository.get(
        ctx, name=fake_repository_name, resource_id=fake_repository_name
    )
    assert ret["result"]
    assert hub.tool.aws.comment_utils.get_empty_comment(
        resource_type="aws.ecr.repository", name=fake_repository_name
    ) in str(ret["comment"])
    assert not ret["ret"]


@pytest.mark.asyncio
@pytest.mark.localstack(False, "LocalStack does not have support for ECR repositories")
async def test_get_repository(hub, ctx, aws_test_ecr_repository):
    ret = await hub.exec.aws.ecr.repository.get(
        ctx, name=aws_test_ecr_repository, resource_id=aws_test_ecr_repository
    )
    assert ret["result"], ret["comment"]
    resource = ret["ret"]
    assert resource
    assert resource.get("registry_id")
    assert aws_test_ecr_repository == resource.get("repository_name")
    assert resource.get("image_tag_mutability")
    assert resource.get("image_scanning_configuration")
    assert resource.get("tags")
