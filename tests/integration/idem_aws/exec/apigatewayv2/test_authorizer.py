import time

import pytest

# Test test_get() is in states/apigatewayv2/test_authorizer.py


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_get_invalid_resource_id(hub, ctx, aws_apigatewayv2_api):
    api_get_name = "idem-test-exec-get-authorizer-" + str(int(time.time()))
    ret = await hub.exec.aws.apigatewayv2.authorizer.get(
        ctx,
        name=api_get_name,
        resource_id="fake-id",
        api_id=aws_apigatewayv2_api["api_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert hub.tool.aws.comment_utils.get_empty_comment(
        resource_type="aws.apigatewayv2.authorizer", name=api_get_name
    ) in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_get_invalid_api_id(hub, ctx, aws_apigatewayv2_api):
    api_get_name = "idem-test-exec-get-authorizer-" + str(int(time.time()))
    ret = await hub.exec.aws.apigatewayv2.authorizer.get(
        ctx, name=api_get_name, resource_id="fake-id", api_id="fake-id"
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert hub.tool.aws.comment_utils.get_empty_comment(
        resource_type="aws.apigatewayv2.authorizer", name=api_get_name
    ) in str(ret["comment"])
