# This SLS file is a meta-state that will find and remove resources created during tests
# There is no configuration needed, simply run `idem state example/cleanup/init.sls`

include:
  - instance
  - subnet
  - vpc
  - network_interface
